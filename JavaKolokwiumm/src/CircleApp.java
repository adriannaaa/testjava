import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;

import javax.swing.*;

public class CircleApp extends JFrame{

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					final CircleApp frame = new CircleApp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public CircleApp(){
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	setSize(450, 350);
	contentPane = new JPanel();
	setContentPane(contentPane);
	contentPane.setLayout(null);

                DrawCircle kanwa = new DrawCircle();
	kanwa.setBounds(1, 1, 400, 200);
	contentPane.add(kanwa);
	SwingUtilities.invokeLater(new Runnable() {
		
		@Override
		public void run() {
			kanwa.initialize();
		}
	});
	JButton btnAdd = new JButton("New Circle");
	btnAdd.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			kanwa.addCircle();
		}
	});
	btnAdd.setBounds(10, 239, 120, 23);
	contentPane.add(btnAdd);
	
	JButton btnMove = new JButton("Move");
	btnMove.addKeyListener(new KeyListener() {
		public void actionPerformed(KeyEvent e) {
			kanwa.moveCircle(e);
		}
	});
			
			
			
	btnAdd.setBounds(10, 239, 120, 23);
	contentPane.add(btnMove);
	
	}
	
}

	
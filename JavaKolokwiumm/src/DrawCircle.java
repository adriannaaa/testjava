import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;

import javax.swing.*;

public class DrawCircle  extends JPanel implements ActionListener{
	
	private static final long serialVersionUID = 1L;
  
Image image;

Graphics2D device;

Graphics2D buffer;

private int delay = 50;

private Timer timer;

private static int numer = 0;

public DrawCircle() {
	super();
	setBackground(Color.WHITE);
	timer = new Timer(delay, this);
}

public void initialize() {
	int width = getWidth();
	int height = getHeight();

	image = createImage(width, height);
	buffer = (Graphics2D) image.getGraphics();
	buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	device = (Graphics2D) getGraphics();
	device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
}

void addCircle() {
	Graphics2D fig = null;

    fig.setPaint(Color.RED);
    fig.draw(new Ellipse2D.Double(50, 50, 250, 250));

//addActionListener(fig);
// Thread(fig).start();
    
    
}


@Override
public void actionPerformed(ActionEvent arg0) {
	// TODO Auto-generated method stub
	
}
public void moveCircle(KeyEvent e) {

    int key = e.getKeyCode();

    int dx;
    int dy;
	if (key == KeyEvent.VK_LEFT) {
        dx = -1;
    }

    if (key == KeyEvent.VK_RIGHT) {
        dx = 1;
    }

    
	if (key == KeyEvent.VK_UP) {
        dy = -1;
    }

    if (key == KeyEvent.VK_DOWN) {
        dy = 1;
    }
}


}
